#ifndef QDEBUG_HELPER_H
#define QDEBUG_HELPER_H

//
// Helper Functions
//
// Copyright (C) 2017 Assured Information Security, Inc. All rights reserved.
//

// ============================================================================
// Includes
// ============================================================================

#include <string>       // std::string
#include <sstream>      // std::stringstream
#include <QtCore>
#include <json.hpp>

// ============================================================================
// Definitions
// ============================================================================

inline QDebug operator<<(QDebug dbg, const std::string& s) {
    dbg << s.c_str();
    return dbg;
}

inline QDebug operator<<(QDebug dbg, const std::stringstream& ss) {
    dbg << ss.str();
    return dbg;
}

inline QDebug operator<<(QDebug dbg, const nlohmann::json json) {
    dbg << json.dump();
    return dbg;
}

#endif // QDEBUG_HELPER_H
